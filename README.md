# Desafio Dito

O projeto foi desenvolvido utilizando [Node.js](https://nodejs.org/) com o framework [Koa.js](http://koajs.com/). O sistema de banco de dados utilizado é o [MongoDB](https://www.mongodb.com/).

## Instalação e execução
Para executar o código basta instalar o [Docker](https://www.docker.com/) e, no diretório raiz do projeto, executar os seguintes comandos:
```
docker-compose up --build
```

O banco de dados será criado em um container e utilizará a pasta db/data para armazenar os dados.
O servidor responde aos pedidos pela porta 3000, logo o cliente pode ser acessado em

```
http://localhost:3000
```

Os dados não ficarão armazenados no computador. Tudo é feito em containers para facilitar a reprodução do código.
Para deletar o banco de dados, basta remover os containers com o seguinte comando:
```
docker-compose rm -f
```

## API
A API possui os seguintes endpoints

#### Obter eventos registrados
```
GET /events
```
Obtém uma lista com todos os eventos registrados (apenas para propósito de visualização).

```
GET http://localhost:3000/api/v1/events
```

Tem como resposta

```
{
  "events": [
    {"kind":"clack","timestamp":"2016-08-26T00:53:11.184Z"},
    {"kind":"clack","timestamp":"2016-08-26T00:40:38.185Z"},
    {"kind":"click","timestamp":"2016-08-26T00:06:38.719Z"}
  ]
}
```

#### Registrar eventos
```
POST /events
```
Registra um evento na API. Requer os seguintes parâmetros:

- **kind**: Uma string contendo o tipo do evento

Por exemplo, o pedido:
```
POST http://localhost:3000/api/v1/events
{
  "kind": "click"
}
```

Registra um evento do tipo *click*.

#### Obter os tipos de eventos registrados
```
GET /events/kinds[?q=filtro]
```
Obtém os tipos de eventos registrados até agora. O parâmetro *q* é opcional na query para filtrar as strings dos tipos.

Por exemplo, o pedido:
```
GET http://localhost:3000/api/v1/events/kinds
```

Tem como resposta
```
{
  "kinds": [
    "clack",
    "click"
  ]
}
```

Equanto que o pedido com filtro:
```
GET http://localhost:3000/api/v1/events/kinds?q=cla
```

Tem como resposta
```
{
  "kinds": [
    "clack"
  ]
}
```
