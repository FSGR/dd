const mongoose = require('mongoose');

/**
 * Modelo dos eventos
 */
const eventSchema = mongoose.Schema({
  kind: String,
  timestamp: Date,
});

const Event = mongoose.model('Event', eventSchema);


module.exports = {
  Event,
};
