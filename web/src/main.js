const koa = require('koa');
const serve = require('koa-static');
const path = require('path');
const mongoose = require('mongoose');

const apiRouter = require('./api');


mongoose.Promise = global.Promise;

// Tenta conectar ao banco de dados
mongoose.connect('mongodb://db/desafio_dito', (err) => {
  if (err) {
    // Loga o erro
    console.error(err);
  } else {
    // Cria o servidor
    const app = koa();

    app.use(apiRouter.routes());
    app.use(serve(path.join(__dirname, '..', 'public')));

    app.listen(80);
  }
});
