const koaRouter = require('koa-router');
const koaBodyParser = require('koa-bodyparser');

const _ = require('lodash');

const { Event } = require('./models');


const router = koaRouter({
  prefix: '/api/v1',
});


// Middlewares da api
router.use(koaBodyParser());


// Rotas da api
/**
 * Retorna todos os eventos registrados.
 */
router.get('/events', function* getEvents() {
  try {
    const events = yield Event.find().sort({ timestamp: 'desc' });

    // Retorna apenas tipo e timestamp
    this.body = {
      events: _.map(events, (e) => _.pick(e, ['kind', 'timestamp'])),
    };
  } catch (e) {
    this.status = 500;
    this.body = {};
  }
});

/**
 * Registra um evento no banco de dados.
 */
router.post('/events', function* postEvents() {
  // Tipo do evento
  const kind = this.request.body.kind;

  if (!kind) {
    this.status = 400;
    this.body = {
      error: {
        message: 'Missing event kind',
        code: 1,
      },
    };
  } else {
    const event = new Event({
      kind: _.trim(kind),
      timestamp: new Date(),
    });

    try {
      yield event.save();
      this.body = {};
    } catch (e) {
      this.status = 500;
      this.body = {};
    }
  }
});

/**
 * Retorna os nomes dos tipos dos eventos registrados.
 * Os nomes retornados são distintos.
 */
router.get('/events/kinds', function* getEventsSearch() {
  // String de filtro dos nomes dos tipos dos eventos
  const queryString = this.request.query.q;

  try {
    // Busca, filtra e agrupa os nomes dos tipos dos eventos
    const kindAggr = yield Event.aggregate([
      { $match: {
        kind: { $regex: new RegExp(`^${queryString}`, 'i') },
      } },
      { $group: {
        _id: '$kind',
      },
    }]);

    // Extrai os tipos dos eventos do resultado do aggregate
    const kinds = _.map(kindAggr, (result) => result._id);

    // Responde com os tipos de eventos encontrados
    this.body = {
      kinds,
    };
  } catch (e) {
    this.status = 500;
    this.body = {};
  }
});


module.exports = router;
