/* eslint-disable */
'use strict';


// Objeto para comunicar com a API
var EventAPI = {
  registerEvent: function(kind, callback) {
    $.post('/api/v1/events', {
      kind: kind,
    })
    .done(function() {
      callback(null);
    })
    .fail(function() {
      callback('error');
    });
  },

  getEvents: function(callback) {
    $.get('/api/v1/events')
    .done(function(data) {
      callback(null, data.events);
    })
    .fail(function() {
      callback('error');
    });
  },
};


// UI dos eventos
$(function() {
  function showRegisteredEvents() {
    EventAPI.getEvents(function(err, events) {
      $('#registered-events').html(_.map(events, JSON.stringify).join('<br/>'));
    });
  }

  // Carrega os eventos já registrados
  showRegisteredEvents();

  // Registra evento ao clicar no botão "Registrar"
  $('#register-event-btn').on('click', function(event) {
    event.preventDefault();

    var eventKind = _.trim($('#register-event-kind').val());

    if (eventKind.length > 0) {
      EventAPI.registerEvent(eventKind, function(err) {
        if (!err) {
          $('#register-event-kind').val('');
          showRegisteredEvents();
        }
      });
    }
  });
});
