/* eslint-disable */
'use strict';


// Configura o autocomplete
$(function() {
  $('.event-kind-select').select2({
    minimumInputLength: 2,

    placeholder: 'Tipo do evento',
    language: 'pt-BR',

    ajax: {
      url: '/api/v1/events/kinds',
      dataType: 'json',

      data: function(params) {
        return {
          q: params.term,
        };
      },

      processResults: function(data, params) {
        return {
          results: _.map(data.kinds, function(kindName, i) { return {id: i + 1, text: kindName} }),
          pagination: {
            more: false,
          },
        };
      },
    },
  });
});
